const db = require("../services/mysql");
var errors = require("restify-errors");

const routes = (server) => {
  server.get("/api/inicio", async (req, res, next) => {
    try {
      if (Object.keys(req.query).length === 0) {
        var startNum = 0;
        var LimitNum = 3;
      } else {
        //parse int Convert String to number
        var startNum = parseInt(req.query.start);
        var LimitNum = parseInt(req.query.limit);
      }
      res.send(await db.home().init(LimitNum, startNum));
    } catch (err) {
      next(err);
    }

    // Exemplo de como usar o token (decoded)
    // try {
    //   const categories = await db.categories().all()
    //   const user = req.decoded
    //   res.send({ categories, user })
    // } catch (error) {
    //   res.send(error)
    // }
    // next()

    // Promisses
    // db.categories().all().then(categories => {
    //   res.send(categories)
    //
    // }).catch(error => {
    //   res.send(error)
    //
    // })
  });
  server.get("/api/slide", async (req, res, next) => {
    try {
      res.send(await db.home().slides());
    } catch (err) {
      next(err);
    }
  });
  server.get("/", (req, res, next) => {
    res.send("Enjoy the silencex...");
    next();
  });
  server.post("/api/auth", async (req, res, next) => {
    if (req.body == null) {
      var { email, password } = req.params;
    } else {
      var { email, password } = req.body;
    }
    try {
      res.send(await db.auth().authenticate(email, password));
    } catch (err) {
      next(err);
    }
  });
  server.get("/api/categoria", async (req, res, next) => {
    try {
      res.send(await db.categoria().all());
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/anime/:id", async (req, res, next) => {
    try {
      res.send(await db.anime().anime(req.params.id));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/episodio/:id/:temporada", async (req, res, next) => {
    try {
      res.send(
        await db.episodio().temporada(req.params.id, req.params.temporada)
      );
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/ep/:id", async (req, res, next) => {
    try {
      res.send(await db.episodio().ep(req.params.id));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/categoria/:categoria", async (req, res, next) => {
    try {
      if (Object.keys(req.query).length === 0) {
        var startNum = 0;
        var LimitNum = 24;
      } else {
        //parse int Convert String to number
        var startNum = parseInt(req.query.start);
        var LimitNum = parseInt(req.query.limit);
      }
      res.send(
        await db
          .categoria()
          .animeCategoria(req.params.categoria, LimitNum, startNum)
      );
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/link/:id", async (req, res, next) => {
    try {
      res.send(await db.episodio().links(req.params.id));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/busca/:nome", async (req, res, next) => {
    try {
      res.send(await db.busca().anime(req.params.nome));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/player/episodios/:id", async (req, res, next) => {
    try {
      res.send(await db.episodio().listeps(req.params.id));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.post(
    "/api/player/error",
    async (req, res, next) => {
      var dados = JSON.parse(req.body);
      req.someData = await db.player().erro(dados.tipo, dados.msg);
    },
    function (req, res, next) {
      res.send(req.someData);
    }
  );
  server.post("/admin/anime", async (req, res, next) => {
    try {
      res.send(await db.anime().listAnime(req.body));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/admin/anime/:id", async (req, res, next) => {
    try {
      var result = await db.anime().animeAdmin(req.params.id);
      res.send(result);
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.post(
    "/admin/anime/:id",
    async (req, res, next) => {
      try {
        var dados = req.body;
        req.someData = await db.anime().editAnime(dados);
        return next();
      } catch (err) {
        next(new errors.NotFoundError({ message: err.error }));
      }
    },
    function (req, res, next) {
      try {
        res.send(req.someData);
      } catch (err) {
        next(new errors.NotFoundError({ message: err.error }));
      }
    }
  );
  server.post(
    "/admin/cad_anime",
    async (req, res, next) => {
      var dados = req.body;
      req.someData = await db.anime().cadAnime(dados);
      return next();
    },
    function (req, res, next) {
      try {
        res.send(req.someData);
      } catch (err) {
        res.send(new errors.NotFoundError({ message: err.error }));
      }
      next();
    }
  );
  server.del("/admin/delanime/:id", async (req, res, next) => {
    try {
      var result = await db.anime().deleteAnime(req.params.id);
      res.send(result);
    } catch (err) {
      res.send(new errors.NotFoundError({ message: err.error }));
    }
    return next();
  });
  //BLOG
  server.get("/api/post/:link", async (req, res, next) => {
    try {
      res.send(await db.blog().post(req.params.link));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/noticias", async (req, res, next) => {

    try {
      if (Object.keys(req.query).length === 0) {
        var startNum = 0;
        var LimitNum = 6;
      } else {
        //parse int Convert String to number
        var startNum = parseInt(req.query.start);
        var LimitNum = parseInt(req.query.limit);
      }
      var respota = await db.blog().postlist(LimitNum, startNum);
      res.send(respota);
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.post("/admin/noticias", async (req, res, next) => {
    try {
      res.send(await db.blog().listpostAdmin(req.body));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.post(
    "/admin/cad_noticia",
    async (req, res, next) => {
      var dados = req.body;
      req.someData = await db.blog().cadastroNoticia(dados);
      return next();
    },
    function (req, res, next) {
      try {
        res.send(req.someData);
      } catch (err) {
        next(new errors.NotFoundError({ message: err.error }));
      }
    }
  );
  server.get("/admin/noticia/:id", async (req, res, next) => {
    try {
      var result = await db.blog().noticia(req.params.id);
      res.send(result);
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.post("/admin/noticia", async (req, res, next) => {
    try {
      res.send(await db.blog().noticiaUpdate(req.body));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.del("/admin/delnoticia/:id", async (req, res, next) => {
    try {
      var result = await db.blog().delNoticia(req.params.id);
      res.send(result);
    } catch (err) {
      res.send(new errors.NotFoundError({ message: err.error }));
    }
    return next();
  });
  //MOVIE
  server.get("/api/movie", async (req, res, next) => {
    try {
      if (Object.keys(req.query).length === 0) {
        var startNum = 0;
        var LimitNum = 24;
      } else {
        //parse int Convert String to number
        var startNum = parseInt(req.query.start);
        var LimitNum = parseInt(req.query.limit);
      }
      res.send(await db.movie().init(LimitNum, startNum));
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/movie/:id", async (req, res, next) => {
    try {
      var result = await db.movie().movie(req.params.id);
      res.send(result);
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });
  server.get("/api/link-movie/:id", async (req, res, next) => {
    try {
      var result = await db.movie().links(req.params.id);
      res.send(result);
    } catch (err) {
      next(new errors.NotFoundError({ message: err.error }));
    }
  });

  //LinkGenerator
  server.get("/blglink/:id/:reso", async (req, res, next) => {
    try {
      var result = await db.lblogModule().link(req.params.id, req.params.reso);
      res.redirect(result, next);
    } catch (err) {
      next(new errors.NotFoundError({
        statusCode: 404
    }, err.error));
    }
  });

  //Episodios ADD
  server.post(
    "/admin/add_epi",
    async (req, res, next) => {
      var dados = req.body;
      req.someData = await db.episodio().cadEP(dados);
      return next();
    },
    function (req, res, next) {
      try {
        res.send(req.someData);
      } catch (err) {
        res.send(new errors.NotFoundError({ message: err.error }));
      }
      next();
    }
  );
};

module.exports = routes;
