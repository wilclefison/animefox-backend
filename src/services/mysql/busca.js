const busca = deps => {
    return {
        anime: (nome) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = `SELECT DISTINCT A.anime_id as id, A.anime_nome as nome, A.anime_imagem as img, A.url, A.anime_ano as ano FROM anime AS A LEFT JOIN animenomes AS AN ON A.anime_id = AN.id WHERE A.anime_nome LIKE ? OR AN.nome LIKE ?`;
                // Mention table from where you want to fetch records example-users
                var table = [`%${nome}%`, `%${nome}%`];
                query = mysqlServer.format(query, table);

                connection.query(query, (error, results) => {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as busca', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    // resolve({ pagination: { page: 1, results: results.length }, categories: results })
                    results.forEach(element => {
                        element.ano = new Date(element.ano).getFullYear()
                    });
                    resolve({ busca: results })
                })
            })
        },

    }
}

module.exports = busca
