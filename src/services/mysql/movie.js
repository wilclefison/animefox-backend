const movie = deps => {
    return {
        init: (LimitNum, startNum) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor

                var query = `SELECT id, nome, img, bg, url FROM ?? WHERE status = 1 ORDER BY id DESC limit ? OFFSET ?`;
                // Mention table from where you want to fetch records example-users
                var table = ['movie', LimitNum, startNum];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, animes) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as categorias', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    var query = `SELECT COUNT(*) AS contagem FROM ?? WHERE status = 1`;
                    // Mention table from where you want to fetch records example-users
                    var table = ['movie'];
                    query = mysqlServer.format(query, table);
                    connection.query(query, function (error, Countanimes) {
                        var obj = { contagem: Countanimes[0].contagem, conteudo: animes }
                        resolve(obj)
                    })

                    // resolve({ pagination: { page: 1, results: results.length }, categories: results })

                });
            })
        },
        movie: (id) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor

                if (isNaN(id)) {
                    var query = "SELECT * FROM ?? WHERE url = ? LIMIT 1";
                } else {
                    var query = "SELECT * FROM ?? WHERE id = ? LIMIT 1";
                }
                // Mention table from where you want to fetch records example-users
                var table = ["movie", id];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, anime) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    if (anime.length > 0) {
                        var query = "SELECT C.categoria_id, C.categoria_nome FROM movie_categoria AS AC INNER JOIN categoria AS C ON AC.c_id = C.categoria_id WHere AC.m_id = ?";
                        // Mention table from where you want to fetch records example-users
                        var table = [anime[0].id];
                        query = mysqlServer.format(query, table);
                        connection.query(query, function (error, categoria) {
                            if (error) {
                                errorHandler(error, 'Falha ao listar as dados 1', reject)
                                return false // para não chegar no resolve e para não termos que usar else
                            }
                            anime[0].categoria = categoria
                            anime[0].data = new Date(anime[0].data).getFullYear()
                            if (categoria.length > 0) {
                                var cat = []
                                for (let index = 0; index < categoria.length; index++) {
                                    cat.push(categoria[index].categoria_id);
                                }
                                var query = "SELECT DISTINCT F.id, F.nome, F.img, F.url FROM movie AS F INNER JOIN movie_categoria AS C ON F.id = C.m_id WHERE C.c_id IN (?) AND F.id <> ? ORDER  BY RAND() LIMIT  4";
                                // Mention table from where you want to fetch records example-users
                                var table = [cat, anime[0].id];
                                query = mysqlServer.format(query, table);
                                connection.query(query, function (error, aleatorio) {
                                    if (error) {
                                        console.log(error)
                                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                                        return false // para não chegar no resolve e para não termos que usar else
                                    }
                                    anime[0].extra = aleatorio;
                                    resolve(anime)
                                })
                            } else {
                                anime[0].extra = null;
                                resolve(anime)
                            }
                        })
                    } else {
                        errorHandler(error, 'Anime Não encontrado', reject)
                    }

                });
            })
        },
        links: (movId) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = "SELECT * FROM ?? WHERE movie = ? ";
                var table = ["movie_link", movId];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, lk) {
                    if (error) {
                        console.log(error)
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }

                    lk = lk.sort((a, b) => { return a.res - b.res; })
                    var links = []
                    for (let i = 0; i < lk.length; i++) {
                        links.push({ prefix: `Opção ${i + 1}`, nome: `${lk[i].tipo}/${lk[i].res}p`, id: lk[i].id, url: lk[i].link, ep: lk[i].episodio, playing: i == 0 })
                    }
                    resolve({ links: links, nextEp: { title: 'Não existe um próximo vídeo.' } })

                })

            })
        },
    }
}



module.exports = movie
