const axios = require("axios").default;
const axiosConfig = {
  headers: {
    Origin: "www.google.com",
    "Content-Type": "application/json;charset=UTF-8",
    "Access-Control-Allow-Origin": "*",
  },
};
const url = "https://www.blogger.com/video.g?token=";

const blogger = (deps) => {
  return {
    link: (t, r) => {
      return new Promise(async (resolve, reject) => {
        const { errorHandler } = deps; // object destructor

        axios
          .get(url + t, axiosConfig)
          .then(async (response) => {
            try {
              //Encontra os Scrpts
              var sc = response.data.match(
                /<script[\s\S]*?>[\s\S]*?<\/script>/gi
              );

              sc = sc.find((element) => {
                return element.match("VIDEO_CONFIG");
              });
              sc = sc.replace(/(<([^>]+)>)/gi, "");
              eval(sc);

              //Check if has VIDEO_CONFIG
              if (typeof VIDEO_CONFIG !== "undefined") {
                var url_real = VIDEO_CONFIG.streams.find((e, index) => {
                  if (e.format_id == r) {
                    return e.play_url;
                  } else {
                    return false;
                  }
                });
                if (typeof url_real === "undefined")
                  url_real = VIDEO_CONFIG.streams[0];

                resolve(url_real.play_url);
              } else {
                reject({ error: "Nada Aqui" });
              }
            } catch (err) {
              errorHandler(err, "Não Link", reject);
            }
          })
          .catch((err) => {
            errorHandler(err, "Não Link", reject);
          });
      });
    },
  };
};

module.exports = blogger;
