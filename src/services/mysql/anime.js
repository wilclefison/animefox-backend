const axios = require('axios');

const { Translate } = require("@google-cloud/translate").v2;

const translate = new Translate();

const traduzir = async (text) => {
  // The target language
  const target = "pt";

  // Translates some text into Russian
  const [translation] = await translate.translate(text, target);
  return translation;
};

const anime = deps => {
  return {
    anime: (id) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps // object destructor

        if(isNaN(id)){
          var query = "SELECT * FROM ?? WHERE url = ? LIMIT 1";
        }else{
          var query = "SELECT * FROM ?? WHERE anime_id = ? LIMIT 1";
        }
        // Mention table from where you want to fetch records example-users
        var table = ["anime", id];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, anime) {
          if (error) {
            errorHandler(error, 'Falha ao listar as dados 1', reject)
            return false // para não chegar no resolve e para não termos que usar else
          }
          if (anime.length > 0) {
            var query = "SELECT P.person_id, P.person_nome FROM anime_person AS AP INNER JOIN person AS P ON AP.person_id = P.person_id WHERE AP.anime_id = ? LIMIT 5";
            // Mention table from where you want to fetch records example-users
            var table = [anime[0].anime_id];
            query = mysqlServer.format(query, table);
            connection.query(query, function (error, person) {
              if (error) {
                errorHandler(error, 'Falha ao listar as dados 1', reject)
                return false // para não chegar no resolve e para não termos que usar else
              }

              var query = "SELECT C.categoria_id, C.categoria_nome FROM anime_categoria AS AC INNER JOIN categoria AS C ON AC.categoria_id = C.categoria_id WHere AC.anime_id = ?";
              // Mention table from where you want to fetch records example-users
              var table = [anime[0].anime_id];
              query = mysqlServer.format(query, table);
              connection.query(query, function (error, categoria) {
                if (error) {
                  errorHandler(error, 'Falha ao listar as dados 1', reject)
                  return false // para não chegar no resolve e para não termos que usar else
                }
                anime[0].categoria = categoria
                anime[0].cast = person
                anime[0].anime_ano = new Date(anime[0].anime_ano).getFullYear()
                resolve(anime)
              })


            })
          } else {
            errorHandler(error, 'Anime Não encontrado', reject)
          }

        });
      })
    },
    listAnime: (dado) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps // object destructor
        var extra = dado.filtered.length === 0 ? `  ` : ` WHERE ${dado.filtered[0].id} LIKE '%${dado.filtered[0].value}%'`
        var query = "SELECT count(*) AS count FROM ??" + extra;
        var queryAnime = "SELECT * FROM ?? " + extra + " LIMIT ?,?";
        // Mention table from where you want to fetch records example-users
        let startIndex = dado.page * dado.pageSize;
        var table = ["anime"];
        var tableAnime = ["anime", startIndex, dado.pageSize]
        query = mysqlServer.format(query, table);
        queryAnime = mysqlServer.format(queryAnime, tableAnime);
        connection.query(query, function (error, countAnime) {
          if (error) {
            errorHandler(error, 'Falha ao listar as dados 1', reject)
            return false // para não chegar no resolve e para não termos que usar else
          }
          connection.query(queryAnime, function (error, anime) {
            if (error) {
              errorHandler(error, 'Falha ao listar as dados 2', reject)
              return false // para não chegar no resolve e para não termos que usar else
            }
            var tatalPages = Math.ceil(parseFloat(countAnime[0].count) / parseFloat(dado.pageSize))
            var obj = { pages: tatalPages, rows: anime }
            resolve(obj)
            //resolve([countAnime[0].count])
          })
        });
      })
    },
    animeAdmin: (id) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps // object destructor
        var query = "SELECT * FROM ?? WHERE anime_id = ? LIMIT 1";
        // Mention table from where you want to fetch records example-users
        var table = ["anime", id];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, anime) {
          if (error) {
            errorHandler(error, 'Falha ao listar as dados 1', reject)
            return false // para não chegar no resolve e para não termos que usar else
          }
          if (anime.length > 0) {
            var query = "SELECT P.person_id, P.person_nome FROM anime_person AS AP INNER JOIN person AS P ON AP.person_id = P.person_id WHERE AP.anime_id = ?";
            // Mention table from where you want to fetch records example-users
            var table = [id];
            query = mysqlServer.format(query, table);
            connection.query(query, function (error, person) {
              if (error) {
                errorHandler(error, 'Falha ao listar as dados 1', reject)
                return false // para não chegar no resolve e para não termos que usar else
              }

              var query = "SELECT C.categoria_id, C.categoria_nome FROM anime_categoria AS AC INNER JOIN categoria AS C ON AC.categoria_id = C.categoria_id WHere AC.anime_id = ?";
              // Mention table from where you want to fetch records example-users
              var table = [id];
              query = mysqlServer.format(query, table);
              connection.query(query, function (error, categoria) {
                if (error) {
                  errorHandler(error, 'Falha ao listar as dados 1', reject)
                  return false // para não chegar no resolve e para não termos que usar else
                }
                var cat = []
                categoria.forEach(element => {
                  cat.push(`${element.categoria_id}`)
                });
                anime[0].categoria = cat
                anime[0].cast = person
                anime[0].anime_ano = new Date(anime[0].anime_ano).toJSON().slice(0, 10)

                resolve(anime)
              })
            })
          } else {
            errorHandler(error, 'Anime Não encontrado', reject)
          }
        });
      })
    },
    editAnime: (dados) => {
      return new Promise((resolve, reject) => {
        try {
          const { connection, errorHandler, mysqlServer } = deps // object destructor
        var query = `UPDATE ?? SET anime_nome = ?, anime_titulo_original = ?, 
        anime_sinopse = ?,
        anime_imagem = ?, anime_temporadas = ?, anime_ano = ?, anime_bg = ?, anime_imdb = ?, 
        anime_situacao = ?, anime_status = ?, gp = ?, url = ? WHERE anime_id = ?`;
        // Mention table from where you want to fetch records example-users
        var table = ["anime", dados.anime_nome, dados.anime_titulo_original, dados.anime_sinopse, dados.anime_imagem, dados.anime_temporadas, dados.anime_ano,
          dados.anime_bg, dados.anime_imdb, dados.anime_situacao, dados.anime_status, dados.gp, dados.url, dados.anime_id];
        query = mysqlServer.format(query, table);
        
        connection.query(query, function (error, countAnime) {
          if (error) {
            errorHandler(error, 'Falha ao listar as dados 1', reject)
            return false // para não chegar no resolve e para não termos que usar else
          }
          if (countAnime.affectedRows > 0) {
            connection.query(`SELECT * FROM anime_categoria WHERE anime_id = ${dados.anime_id}`, function (error, Categories) {
              if (error) {
                errorHandler(error, 'Falha ao listar as dados 2', reject)
                return false // para não chegar no resolve e para não termos que usar else
              }
              var adiciona = []
              var remove = []
              if (Categories.length !== 0) {
                Categories.forEach(cat => {
                  if (dados.categoria.find(x => parseInt(x) == parseInt(cat.categoria_id)) == undefined) {
                    remove.push(cat.id)
                  } else {
                    var index = dados.categoria.indexOf(`${cat.categoria_id}`);
                    dados.categoria.splice(index, 1);
                  }
                });
              }
              if (remove.length != 0)
                remove.forEach(id => {
                  connection.query(`DELETE FROM anime_categoria WHERE id = ${id}`)
                });
              if (dados.categoria.length != 0)
                dados.categoria.forEach(id => {
                  connection.query(`INSERT INTO anime_categoria (id, anime_id, categoria_id) VALUES (NULL, ${dados.anime_id}, ${id})`)
                });
              resolve({ msg: 'Anime Editado' })
            })
          } else {
            errorHandler(error, 'Anime não foi alterado #01', reject)
          }
        });
        } catch (error) {
          errorHandler(error, 'Anime não foi alterado #00', reject)

        }
      });
    },
    cadAnime: (dados) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps // object destructor
        var query = `INSERT INTO ?? (anime_id, anime_nome, anime_titulo_original, anime_sinopse, 
        anime_imagem, anime_temporadas, anime_ano, anime_bg, anime_imdb, anime_situacao, anime_status, gp, url) 
        VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
        var table = ["anime", dados.anime_nome, dados.anime_titulo_original, dados.anime_sinopse, dados.anime_imagem, dados.anime_temporadas,
          dados.anime_ano, dados.anime_bg, dados.anime_imdb, dados.anime_situacao, dados.anime_status, dados.gp, dados.url];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, countAnime) {
          if (error) {
            errorHandler(error, 'Falha ao Cadastrar', reject)
            return false // para não chegar no resolve e para não termos que usar else
          }
          if (countAnime.affectedRows > 0) {
            if (dados.categoria.length != 0)
              dados.categoria.forEach(id => {
                connection.query(`INSERT INTO anime_categoria (id, anime_id, categoria_id) VALUES (NULL, ${countAnime.insertId}, ${id})`)
              });
            const leng = 'pt-BR';
            //Atualizar todos os episodios.
            try {
              for (let temporada = 1; temporada <= dados.anime_temporadas; temporada++) {
                axios.get(`https://api.themoviedb.org/3/tv/${dados.anime_imdb}/season/${temporada}?api_key=3f62f8dc02f417831a625c5caff08c15&language=${leng}`).then(async (response) => {
                  if (response.status === 200) {
                    var obj = response.data
                    obj.episodes.forEach((episodio, index) => {
                      if (new Date(episodio.air_date) <= new Date() && episodio.air_date != null)
                        if (episodio.overview == '' || episodio.name.includes('Episódio')) {
                          axios.get(`https://api.themoviedb.org/3/tv/${dados.anime_imdb}/season/${temporada}/episode/${episodio.episode_number}?api_key=3f62f8dc02f417831a625c5caff08c15&language=en-US`).then(async (response) => {
                            if (response.status === 200) {
                              var obj = response.data
                              var traducao = obj.overview != '' ? await traduzir(obj.overview) : 'Sem descrição no momento.';
                              var traducaoNome = obj.name.includes('Episode') ? !episodio.name.includes('Episódio') ? episodio.name : `Episódio ${episodio.episode_number}` : !episodio.name.includes('Episódio') ? episodio.name : await traduzir(obj.name)
                              if (traducao.length > 235) {
                                traducao = traducao.substr(0, (236 + traducao.substr('235').indexOf(".")))
                              }
                              connection.query(`SELECT * FROM episodio WHERE numero = ${episodio.episode_number} and anime = ${countAnime.insertId} and temporada = ${temporada}`, function (error, ep) {

                                if (ep.length > 0) {
                                  var query = `UPDATE episodio SET nome = ?, descricao = ?, air_date = ${episodio.air_date == null ? 'NULL' : `'${episodio.air_date}'`}, ${episodio.still_path != undefined ? `img = 'https://image.tmdb.org/t/p/w500${episodio.still_path}',` : ''} duracao = 25 WHERE  numero = ${episodio.episode_number} and anime = ${countAnime.insertId} and temporada = ${temporada}`
                                  query = mysqlServer.format(query, [traducaoNome, traducao]);
                                  connection.query(query)
                                } else {
                                  var query = `INSERT INTO episodio (nome, descricao, img, air_date, anime, numero, temporada, duracao) VALUES (?, ?, '${episodio.still_path != undefined ? `https://image.tmdb.org/t/p/w500${episodio.still_path}` : 'https://prgbrasil.com/wp-content/themes/consultix/images/no-image-found-360x250.png'}',
                                                            ${episodio.air_date == null ? 'NULL' : `'${episodio.air_date}'`}, '${countAnime.insertId}', '${episodio.episode_number}', '${temporada}', '25')`
                                  query = mysqlServer.format(query, [traducaoNome, traducao]);
                                  connection.query(query)
                                }
                              })
                            }
                          });
                        } else {
                          if (episodio.overview.length > 235) {
                            episodio.overview = episodio.overview.substr(0, (236 + episodio.overview.substr('235').indexOf(".")))
                          }
                          connection.query(`SELECT * FROM episodio WHERE numero = ${episodio.episode_number} and anime = ${countAnime.insertId} and temporada = ${temporada}`, function (error, ep) {
                            if (ep.length > 0) {
                              var query = `UPDATE episodio SET nome = ?, descricao = ?, air_date = ${episodio.air_date == null ? 'NULL' : `'${episodio.air_date}'`}, ${episodio.still_path != undefined ? `img = 'https://image.tmdb.org/t/p/w500${episodio.still_path}',` : ''} duracao = 25 WHERE  numero = ${episodio.episode_number} and anime = ${countAnime.insertId} and temporada = ${temporada}`
                              query = mysqlServer.format(query, [episodio.name, episodio.overview]);
                              connection.query(query)
                            } else {
                              var query = `INSERT INTO episodio (nome, descricao, img, air_date, anime, numero, temporada, duracao) VALUES (?, ?, '${episodio.still_path != undefined ? `https://image.tmdb.org/t/p/w500${episodio.still_path}` : 'https://prgbrasil.com/wp-content/themes/consultix/images/no-image-found-360x250.png'}', ${episodio.air_date == null ? 'NULL' : `'${episodio.air_date}'`}, '${countAnime.insertId}', '${episodio.episode_number}', '${temporada}', '25')`
                              query = mysqlServer.format(query, [episodio.name, episodio.overview]);
                              connection.query(query)
                            }
                          })
                        }

                    });
                  }
                }).catch((erro) => {
                  console.log(erro)
                })
              }
            } catch (error) {
              console.log(erro)
            }

            resolve(countAnime)
          } else {
            errorHandler(error, 'Falha ao Cadastrar', reject)
          }
        })
      });
    },
    deleteAnime: (id) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps // object destructor
        var query = `DELETE FROM ?? WHERE anime_id = ?`;
        // Mention table from where you want to fetch records example-users
        var table = ["anime", id];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, countAnime) {
          if (error) {
            errorHandler(error, 'Falha ao listar as dados 1', reject)
            return false // para não chegar no resolve e para não termos que usar else
          }
          if (countAnime.affectedRows > 0) {
            resolve({ msg: 'Anime Editado' })
          } else {
            errorHandler(error, 'Anime não foi alterado', reject)
          }
        });
      });
    }
  }
}

module.exports = anime
