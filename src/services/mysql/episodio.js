const checkRes = (r) => {
  switch (r) {
    case "1080":
      return "FHD";
    case "780":
      return "HD";
    default:
      return "SD";
  }
};
const episodio = (deps) => {
  return {
    ep: (id) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps; // object destructor
        var query = "SELECT * FROM ?? WHERE id = ?";
        // Mention table from where you want to fetch records example-users
        var table = ["episodio", id];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, episodio) {
          if (error) {
            errorHandler(error, "Falha ao listar as dados 1", reject);
            return false; // para não chegar no resolve e para não termos que usar else
          }
          resolve(episodio);
        });
      });
    },
    temporada: (anime_id, temporada) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps; // object destructor
        var query =
          "SELECT * FROM ?? WHERE anime = ? AND temporada = ? ORDER BY numero ASC";
        // Mention table from where you want to fetch records example-users
        var table = ["episodio", anime_id, temporada];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, episodio) {
          if (error) {
            errorHandler(error, "Falha ao listar as dados 1", reject);
            return false; // para não chegar no resolve e para não termos que usar else
          }
          resolve(episodio);
        });
      });
    },
    links: (episodioId) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps; // object destructor
        var query = "SELECT * FROM ?? WHERE episodio = ? ";
        var table = ["link", episodioId];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, lk) {
          if (error) {
            errorHandler(error, "Falha ao listar as dados 1", reject);
            return false; // para não chegar no resolve e para não termos que usar else
          }

          lk = lk.sort((a, b) => {
            return a.resolucao - b.resolucao;
          });
          //Procura o Proximo EP
          var links = [];
          for (let i = 0; i < lk.length; i++) {
            links.push({
              prefix: `Opção ${i + 1}`,
              nome: `${lk[i].tipo}/${lk[i].resolucao}p`,
              id: lk[i].id,
              url: lk[i].link,
              ep: lk[i].episodio,
              playing: i == 0,
            });
          }
          connection.query(
            `SELECT id, nome, numero, temporada, descricao, img FROM episodio WHERE numero = (SELECT numero + 1 FROM episodio WHERE id = ${episodioId})
                    AND anime = (SELECT anime FROM episodio WHERE id = ${episodioId}) AND temporada = (SELECT temporada FROM episodio WHERE id = ${episodioId})`,
            function (error, nextEp) {
              if (error) {
                errorHandler(error, "Falha ao listar as dados 1", reject);
                return false; // para não chegar no resolve e para não termos que usar else
              }

              if (nextEp.length > 0) {
                resolve({
                  links: links,
                  nextEp: {
                    id: nextEp[0].id,
                    title: nextEp[0].nome,
                    number: nextEp[0].numero,
                    session: nextEp[0].temporada,
                    description: nextEp[0].descricao.substr(0, 70) + "...",
                    img: nextEp[0].img,
                  },
                });
              } else {
                connection.query(
                  `SELECT id, nome, numero, temporada, descricao, img FROM episodio WHERE numero = 1
                            AND anime = (SELECT anime FROM episodio WHERE id = ${episodioId}) AND temporada = (SELECT (temporada + 1) FROM episodio WHERE id = ${episodioId})`,
                  function (error, nextEpT) {
                    if (nextEpT.length > 0) {
                      resolve({
                        links: links,
                        nextEp: {
                          id: nextEpT[0].id,
                          title: nextEpT[0].nome,
                          number: nextEpT[0].numero,
                          session: nextEpT[0].temporada,
                          description:
                            nextEpT[0].descricao.substr(0, 70) + "...",
                          img: nextEpT[0].img,
                        },
                      });
                    } else {
                      resolve({
                        links: links,
                        nextEp: { title: "Não existe um próximo vídeo." },
                      });
                    }
                  }
                );
              }
            }
          );
        });
      });
    },
    listeps: (animeId, episodioId) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps; // object destructor
        var query =
          "SELECT id, nome, numero, temporada, img FROM ?? WHERE anime = ? ORDER BY temporada ASC, numero ASC";
        var table = ["episodio", animeId];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, eps) {
          if (error) {
            errorHandler(error, "Falha ao listar as dados 1", reject);
            return false; // para não chegar no resolve e para não termos que usar else
          }
          if (eps.length > 0) {
            resolve(eps);
          } else {
            reject([]);
          }
        });
      });
    },
    cadEP: (dados) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps; // object destructor
        var query = `INSERT INTO ?? (id, nome, descricao, img, air_date, anime, numero, temporada, duracao) 
              VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)`;
        var table = [
          "episodio",
          dados.nome,
          dados.descricao,
          dados.img,
          dados.air_data,
          dados.anime,
          dados.numero,
          dados.temporada,
          dados.duracao,
        ];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, countAnime) {
          if (error) {
            errorHandler(error, "Falha ao Cadastrar", reject);
            return false; // para não chegar no resolve e para não termos que usar else
          }
          if (countAnime.affectedRows > 0) {
            if (dados.links.length != 0)
              dados.links.forEach((dt) => {
                connection.query(
                  `INSERT INTO link (id, episodio, link, tipo, resolucao) VALUES (NULL, '${
                    countAnime.insertId
                  }', '${dt.url}', '${checkRes(dt.res)}', '${dt.res}')`
                );
              });

            resolve(countAnime);
          } else {
            errorHandler(error, "Falha ao Cadastrar", reject);
          }
        });
      });
    },
  };
};

module.exports = episodio;
