const player = deps => {
    return {
        erro: (tipo, msg) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = "INSERT INTO ?? (`id`, `tipo`, `msg`, `data`) VALUES (NULL, ?, ?, ?)";
                // Mention table from where you want to fetch records example-users
                var table = ["errors", tipo, msg, (new Date())];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, results) {
                    if (error || !results.affectedRows) {
                        errorHandler(error, `Falha ao enviar notificação de problemas`, reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    resolve({ tipo, msg, affectedRows: results.affectedRows })
                })
            })
        }
    }
}

module.exports = player
