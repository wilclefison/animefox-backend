const htmlToText = require('html-to-text');

const blog = deps => {
    return {
        post: (link) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = `SELECT * FROM ?? WHERE link = ?`;
                // Mention table from where you want to fetch records example-users
                var table = ['noticias', link];
                query = mysqlServer.format(query, table);
                connection.query(query, (error, results) => {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as busca', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    if (results.length > 0) {
                        var noticia = results[0]
                        query = `SELECT tags.* FROM tags INNER JOIN noticias_tags ON  tags.Tag_ID = noticias_tags.tag_id
                        WHERE noticias_tags.noticia_id = ${noticia.id}`;
                        connection.query(query, (error, tags) => {
                            if (error) {
                                errorHandler(error, 'Falha ao listar as busca', reject)
                                return false // para não chegar no resolve e para não termos que usar else
                            }
                            noticia.tags = tags;

                            //Selecionar ultimas noticias
                            query = `SELECT titulo, img, link FROM noticias ORDER BY id DESC LIMIT 5`;
                            connection.query(query, (error, last) => {
                                last.forEach(nt => {
                                    nt.img = nt.img.replace('.png', 'l.png')
                                    nt.img = nt.img.replace('.jpg', 'l.jpg')
                                    nt.img = nt.img.replace('.gif', 'l.gif')
                                });
                                query = `SELECT titulo, img, link FROM noticias ORDER BY rand() LIMIT 4`;
                                connection.query(query, (error, aleatorio) => {
                                    aleatorio.forEach(nt => {
                                        nt.img = nt.img.replace('.png', 'l.png')
                                        nt.img = nt.img.replace('.jpg', 'l.jpg')
                                        nt.img = nt.img.replace('.gif', 'l.gif')
                                    });
                                    noticia.ultimos = last;
                                    noticia.mais = aleatorio;

                                    resolve(noticia)
                                })
                            })
                        })
                    } else {
                        errorHandler(error, 'Não Encontrado', reject)
                    }
                })
            })
        },
        postlist: (LimitNum, startNum) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps
                var query1 = "SELECT count(*) AS count FROM ??";
                var query = "SELECT id, titulo, img, dia, link, destaque FROM ?? ORDER BY id DESC limit ? OFFSET ?";
                var table = ["noticias", LimitNum, startNum];
                var table1 = ["noticias"];

                query = mysqlServer.format(query, table);
                query1 = mysqlServer.format(query1, table1);
               
                connection.query(query1, function (error, countnews) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    var tatalPages = Math.ceil(parseFloat(countnews[0].count) / parseFloat(LimitNum))
                    connection.query(query, function (error, noticias) {
                        if (error) {
                            errorHandler(error, 'Falha ao listar as dados 1', reject)
                            return false // para não chegar no resolve e para não termos que usar else
                        }
                        var tags_id = []
                        noticias.forEach(nt => {
                            tags_id.push(nt.id)
                            nt.img = nt.img.replace('.png', 'l.png')
                            nt.img = nt.img.replace('.jpg', 'l.jpg')
                            nt.img = nt.img.replace('.gif', 'l.gif')
                            nt.tags = []
                        });
                        var query = `SELECT * FROM tags INNER JOIN noticias_tags ON tags.Tag_ID = noticias_tags.tag_id
                        WHERE noticias_tags.noticia_id IN (${tags_id.join(',')})`;
                        connection.query(query, function (error, tags) {
                            if (error) {
                                errorHandler(error, 'Falha ao buscar tags caregorias', reject)
                                return false // para não chegar no resolve e para não termos que usar else
                            }
                            if (tags.length > 0) {
                                noticias.forEach(nt => {
                                    tags.map(x => {
                                        if (x.noticia_id == nt.id) {
                                            nt.tags.push(x)
                                        }
                                    })
                                })
                            }
                            if (startNum == 0) {
                                var query = `SELECT id, titulo, img, dia, link, destaque FROM noticias WHERE destaque = 1 ORDER BY id DESC LIMIT 5`;
                                connection.query(query, function (error, destaques) {
                                    if (error) {
                                        errorHandler(error, 'Falha ao buscar destaques', reject)
                                        return false // para não chegar no resolve e para não termos que usar else
                                    }

                                    destaques.forEach(nt => {
                                        nt.tags = []
                                    });
                                    resolve({ pages: tatalPages, data: noticias, destaque: destaques })

                                });
                            } else {
                                resolve({ pages: tatalPages, data: noticias })
                            }
                        })
                    })

                });


            });
        },
        listpostAdmin: (dado) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var extra = dado.filtered.length === 0 ? `  ` : ` WHERE ${dado.filtered[0].id} LIKE '%${dado.filtered[0].value}%'`
                var query = "SELECT count(*) AS count FROM ??" + extra;
                var queryAnime = "SELECT * FROM ?? " + extra + "ORDER BY id DESC LIMIT ?,?";
                // Mention table from where you want to fetch records example-users
                let startIndex = dado.page * dado.pageSize;
                var table = ["noticias"];
                var tableAnime = ["noticias", startIndex, dado.pageSize]
                query = mysqlServer.format(query, table);
                queryAnime = mysqlServer.format(queryAnime, tableAnime);
                connection.query(query, function (error, countAnime) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    connection.query(queryAnime, function (error, anime) {
                        if (error) {
                            errorHandler(error, 'Falha ao listar as dados 2', reject)
                            return false // para não chegar no resolve e para não termos que usar else
                        }
                        var tatalPages = Math.ceil(parseFloat(countAnime[0].count) / parseFloat(dado.pageSize))
                        var obj = { pages: tatalPages, rows: anime }
                        resolve(obj)
                    })
                });
            })
        },
        cadastroNoticia: (dado) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = `INSERT INTO ?? (id, titulo, img, conteudo, dia, fonte, link, destaque) VALUES (NULL, ?, ?, ?, ?, ?, ?, ?)`;
                var table = ["noticias", dado.titulo, dado.img, dado.conteudo, new Date(), dado.fonte, dado.link, dado.destaque];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, countAnime) {
                    if (error) {
                        errorHandler(error, 'Falha ao Cadastrar', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    if (countAnime.affectedRows > 0) {
                        resolve(countAnime)
                    } else {
                        errorHandler(error, 'Falha ao Cadastrar', reject)
                    }
                })
            });
        },
        noticia: (id) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = "SELECT * FROM ?? WHERE id = ? LIMIT 1";
                // Mention table from where you want to fetch records example-users
                var table = ["noticias", id];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, noticias) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    if (noticias.length > 0) {
                        resolve(noticias)
                    } else {
                        errorHandler(error, 'Noticia Não encontrado', reject)
                    }
                });
            })
        },
        noticiaUpdate: (dados) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = `UPDATE ?? SET titulo = ?, img = ?, 
                conteudo = ?,  fonte = ?, link = ?, destaque = ? WHERE id = ?`;
                // Mention table from where you want to fetch records example-users
                var table = ["noticias", dados.titulo, dados.img, dados.conteudo, dados.fonte, dados.link, dados.destaque, dados.id];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, countNoticia) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    if (countNoticia.affectedRows > 0) {
                        resolve({ msg: 'Noticia Editada' })
                    }
                });
            });
        },
        delNoticia: (id) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = `DELETE FROM ?? WHERE id = ?`;
                // Mention table from where you want to fetch records example-users
                var table = ["noticias", id];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, countAnime) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    if (countAnime.affectedRows > 0) {
                        resolve({ msg: 'Noticia Deletada' })
                    } else {
                        errorHandler(error, 'Anime não foi alterado', reject)
                    }
                });
            });
        }
    }
}

module.exports = blog
