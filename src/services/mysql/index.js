
const mysqlServer = require('mysql')

const connection = mysqlServer.createConnection({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  port: process.env.MYSQL_PORT
})

const errorHandler = (error, msg, rejectFunction) => {
  rejectFunction({ error: msg })
}

const homeModule = require('./home')({ connection, errorHandler, mysqlServer })
const categoriaModule = require('./categories')({ connection, errorHandler, mysqlServer })
const usersModule = require('./users')({ connection, errorHandler })
const authModule = require('./auth')({ connection, errorHandler })
const animeModule = require('./anime')({ connection, errorHandler, mysqlServer })
const episodioModule = require('./episodio')({ connection, errorHandler, mysqlServer })
const buscaModule = require('./busca')({ connection, errorHandler, mysqlServer })
const playerModule = require('./player')({ connection, errorHandler, mysqlServer })
const blogModule = require('./blog')({ connection, errorHandler, mysqlServer })
const movieModule = require('./movie')({ connection, errorHandler, mysqlServer })
const lblogModule = require('./links/blogger')({ connection, errorHandler, mysqlServer })

module.exports = {
  home: () => homeModule,
  users: () => usersModule,
  auth: () => authModule,
  categoria: () => categoriaModule,
  anime: () => animeModule,
  episodio: () => episodioModule,
  busca: () => buscaModule,
  player: () => playerModule,
  blog: () => blogModule,
  movie: () => movieModule,
  lblogModule: () => lblogModule,
}
