const home = deps => {
    return {
        init: (LimitNum, startNum) => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = "SELECT * FROM ?? ORDER BY categoria_nome ASC limit ? OFFSET ?";
                // Mention table from where you want to fetch records example-users
                var table = ["categoria", LimitNum, startNum];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, categorias) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    var date = []

                    if (startNum == 0) {

                        var query = `SELECT EP.id, EP.nome as ep, EP.img, AN.anime_nome as nome, EP.air_date as data, 
                        EP.numero as numero, AN.url FROM episodio AS EP INNER JOIN anime AS AN ON AN.anime_id = EP.anime 
                        ORDER BY EP.air_date DESC, EP.id DESC  LIMIT 14`;
                        connection.query(query, function (error, episodios) {
                            var animeObj = {
                                categoria: 'Novos Episódios',
                                tipo: 1,
                                conteudo: episodios,
                            }
                            date.push(animeObj)
                        });

                        var query = `SELECT id, titulo, img, dia,  link FROM noticias ORDER BY id DESC limit 6`;
                        connection.query(query, function (error, news) {
                            news.forEach(nt => {
                                nt.img = nt.img.replace('.png', 'l.png')
                                nt.img = nt.img.replace('.jpg', 'l.jpg')
                                nt.img = nt.img.replace('.gif', 'l.gif')
                            })
                            var animeObj = {
                                categoria: 'Notícias',
                                tipo: 3,
                                conteudo: news,
                            }
                            date.push(animeObj)
                        });


                    }
                    categorias.forEach((cat, index, array) => {
                        var query = `SELECT A.anime_nome as nome, A.anime_imagem as img , A.url FROM anime AS A 
                        INNER JOIN anime_categoria AS AC ON A.anime_id = AC.anime_id
                        WHERE AC.categoria_id = ${cat.categoria_id} ORDER BY A.anime_id DESC LIMIT 12`;
                        connection.query(query, function (error, animes) {
                            if (error) {
                                errorHandler(error, 'Falha ao listar as dados 2', reject)
                                return false // para não chegar no resolve e para não termos que usar else
                            }
                            var animeObj = {
                                categoria: cat.categoria_nome,
                                tipo: 2,
                                conteudo: animes
                            }
                            date.push(animeObj)
                            if (index === array.length - 1) resolve(date);
                        });

                    });
                    // resolve({ pagination: { page: 1, results: results.length }, categories: results })
                })
            })
        },
        slides: () => {
            return new Promise((resolve, reject) => {
                const { connection, errorHandler, mysqlServer } = deps // object destructor
                var query = "SELECT id, titulo, descri, img, link, type FROM ?? WHERE status = 1 ORDER BY id DESC LIMIT 4";
                // Mention table from where you want to fetch records example-users
                var table = ["slides"];
                query = mysqlServer.format(query, table);
                connection.query(query, function (error, sd) {
                    if (error) {
                        errorHandler(error, 'Falha ao listar as dados 1', reject)
                        return false // para não chegar no resolve e para não termos que usar else
                    }
                    resolve(sd)
                })
            })
        }
    }
}



module.exports = home
