const categories = deps => {
  return {
    all: () => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps // object destructor
        connection.query(`SELECT C.categoria_id AS id, C.categoria_nome as categoria FROM categoria AS C ORDER BY C.categoria_nome ASC`, (error, results) => {
          if (error) {
            errorHandler(error, 'Falha ao listar as categorias', reject)
            return false // para não chegar no resolve e para não termos que usar else
          }
          // resolve({ pagination: { page: 1, results: results.length }, categories: results })
          resolve({ categories: results })
        })
      })
    },
    animeCategoria: (cat, LimitNum, startNum) => {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler, mysqlServer } = deps // object destructor

        var query = `SELECT * FROM anime_categoria AS AC 
        INNER JOIN anime AS A ON AC.anime_id = A.anime_id 
        INNER JOIN categoria AS C ON C.categoria_id = AC.categoria_id 
        WHERE C.categoria_nome LIKE ? ORDER BY A.anime_ano DESC limit ? OFFSET ?`;
        // Mention table from where you want to fetch records example-users
        var table = [cat, LimitNum, startNum];
        query = mysqlServer.format(query, table);
        connection.query(query, function (error, animes) {
          if (error) {
            errorHandler(error, 'Falha ao listar as categorias', reject)
            return false // para não chegar no resolve e para não termos que usar else
          }
          var query = `SELECT COUNT(*) AS contagem FROM anime_categoria AS AC 
          INNER JOIN anime AS A ON AC.anime_id = A.anime_id 
          INNER JOIN categoria AS C ON C.categoria_id = AC.categoria_id 
          WHERE C.categoria_nome LIKE ?`;
          // Mention table from where you want to fetch records example-users
          var table = [cat];
          query = mysqlServer.format(query, table);
          connection.query(query, function (error, Countanimes) {
            var obj = { contagem: Countanimes[0].contagem, conteudo: animes }
            resolve(obj)
          })

          // resolve({ pagination: { page: 1, results: results.length }, categories: results })
          
        });
      })

    
    }
  }
}

module.exports = categories
